package com.example.demo.controller;


import com.example.demo.models.Person;
import com.example.demo.services.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonServiceImpl personService;

    /*
    @GetMapping()
    public List<Person> getPersons() {
        return personService.getPersons();
    }

    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable Integer id) {
        return personService.getPersonById(id);
    }

    @PostMapping()
    public Person savePerson(@RequestBody Person person) {
        return personService.savePerson(person);
    }

    @DeleteMapping("/{id}")
    public String deletePerson(@PathVariable Integer id) {
        return personService.deletePerson(id);
    }
    */

    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

}
