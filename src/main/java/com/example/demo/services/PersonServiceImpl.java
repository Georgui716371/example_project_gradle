package com.example.demo.services;


import com.example.demo.models.Person;
import com.example.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements IPerson  {
    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    @Override
    public Person getPersonById(Integer id) {
        return personRepository.findById(id).orElse(null);
    }

    @Override
    public Person savePerson(Person person) {

        Person personNew = personRepository.save(person);
        return personNew;
    }

    @Override
    public String deletePerson(Integer id) {
        return personRepository.findById(id).map(person -> {
            personRepository.delete(person);
            return "Deleted Successfully!";
        }).orElse("Person not found!");
    }

}
