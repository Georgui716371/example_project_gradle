package com.example.demo.services;

import com.example.demo.models.Person;

import java.util.List;

public interface IPerson {

    List<Person> getPersons();

    Person getPersonById(Integer id);

    Person savePerson(Person person);

    String deletePerson(Integer id);
}
